# Convert OWL ontology to HTML format using pyLODE.
We have customized the [original templates](https://github.com/rdflib/pyLODE) that will convert a scheme of our [OntoAvida](https://gitlab.com/fortunalab/ontoavida), an ontology written in the OWL language, in a html file so that its classes, object properties, and datatype properties can be easily visualized.

## 1). Install pyLODE using pip3:
> install pylode and its dependencies from the repository.

``
pip3 install pylode
``
## 2). Run pyLODE from the source code:
> first way to run pyLODE.

### 2.1). Clone the repository:

``
git clone https://gitlab.com/fortunalab/pyLODE.git
``

### 2.2). Convert ontology to a html file:

``
cd pyLODE/
``

``
python3 pylode/cli.py -i ontoavida_LODE.owl -o ontoavida_LODE.html
``

## 3). Run pyLODE from the binary we have to build:
> this alternative way or running pyLODE allows us not to rely on the fork or any remote repository (we can edit any file to customize the template at *pyLODE/pylode/* and build the binary again).

### 3.1). Install pyinstaller:

``
sudo pip3 install pyinstaller
``

### 3.2). Build binary (pyLODE script will be placed within *dist* folder):
> we have to build the binary every time we customize/edit the template.

``
cd pylode/
``

``
sudo pyinstaller pyLODE-cli.spec
``

### 3.3). Write the ontology as html using the binary:

``
sudo chmod +x dist/pyLODE
``

``
dist/pyLODE -i ../ontoavida_LODE.owl -o ../ontoavida_LODE.html
``

## 4). Source code:
These customized pyLODE templates were developed by [Raúl Ortega](https://gitlab.com/raul.ortega).

---
